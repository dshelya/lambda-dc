package dc.lambda;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import dc.lambda.connection.ConnectionMetadata;
import dc.lambda.connection.ConnectionRef;
import dc.lambda.connection.factories.DatabaseFactoryInitException;
import dc.lambda.connection.factories.H2FileFactory;
import dc.lambda.connection.factories.H2InMemoryFactory;

@RunWith(MockitoJUnitRunner.class)
public class SelectTest {	
	
	private static class Entity {
		public void setColumn1(String some) {

		}

		void setColumn2(int some) {

		}
	}
	
	@BeforeClass
	public static void setup() throws DatabaseFactoryInitException {
		//Registers system in-memory database and set it as default (only because it was first time)
		ConnectionMetadata.registerH2InMemoryDatabase(); 

		//Equivalent way to ConnectionMetadata.register(H2InMemoryFactory.reify(), true)
		ConnectionMetadata.registerH2InMemoryDatabase().setAsDefault(); 
	}
	
	@Test
	@Ignore
	public void test$0UseSpecificDatabaseReference() throws DatabaseFactoryInitException {
		//SELECT COLUMN1, COLUMN2 FROM TEST T
		//JOIN OTHER O ON O.ID = T.AID 
		//LEFT JOIN ANOTHER A ON A.SOMEFILED = T.SOMEFILED 		
		//WHERE COLUMN3 = '' AND COLUMN4 IS NOT NULL
		//ORDER BY COLUMN1
		
		ConnectionRef h2file = ConnectionMetadata.registerH2FileDatabase("./database.mv.db");
		
		List<Entity> data = new ArrayList<>();		
		DB.selectInto(data)
			.from("TEST")
			.join("OTHER").on("ID", "AID")
			.leftJoin("ANOTHER").on("ID")			
			.bind("COLUMN1", Entity::setColumn1)
			.bind("COLUMN2", Entity::setColumn2)
 			.where("COLUMN3 = '' AND COLUMN4 IS NOT NULL")
 			.orderBy("COLUMN1")
 			.use(h2file)
			.reify();
	}

	@Test
	public void test() {
		//SELECT COLUMN1, COLUMN2 FROM TEST T
		//JOIN OTHER O ON O.ID = T.AID 
		//LEFT JOIN ANOTHER A ON A.SOMEFILED = T.SOMEFILED 		
		//WHERE COLUMN3 = '' AND COLUMN4 IS NOT NULL
		//ORDER BY COLUMN1
		
		List<Entity> data = new ArrayList<>();		
		DB.selectInto(data)
			.from("TEST")
			.join("OTHER").on("ID", "AID")
			.leftJoin("ANOTHER").on("ID")			
			.bind("COLUMN1", Entity::setColumn1)
			.bind("COLUMN2", Entity::setColumn2)
 			.where("COLUMN3 = '' AND COLUMN4 IS NOT NULL")
 			.orderBy("COLUMN1")
			.reify();
	}
	
	@Test
	public void test2() {
		//SELECT DISTINCT COLUMN1, COLUMN2 FROM TEST T
		//JOIN OTHER O ON O.ID = T.AID 
		//LEFT JOIN ANOTHER A ON A.SOMEFILED = T.SOMEFILED 		
		//WHERE COLUMN3 = '' AND COLUMN4 IS NOT NULL
		//ORDER BY COLUMN1
 				
		List<Entity> data1 = DB.<Entity>select()
			.from("TEST")
			.join("OTHER").on("ID", "AID")
			.leftJoin("ANOTHER").on("ID")			
			.bind("COLUMN1", Entity::setColumn1)
			.bind("COLUMN2", Entity::setColumn2)
 			.where("COLUMN3 = '' AND COLUMN4 IS NOT NULL")
 			.orderBy("COLUMN1")
 			.distinct()
			.reify();
	}
	
	@Test
	public void test3() {
		//SELECT DISTINCT COLUMN1, COLUMN2 FROM TEST T
		//JOIN OTHER O ON O.ID = T.AID 
		//LEFT JOIN ANOTHER A ON A.SOMEFILED = T.SOMEFILED 		
		//WHERE COLUMN3 = '' AND COLUMN4 IS NOT NULL
		//ORDER BY COLUMN1
		//LIMIT 1
		
		Entity data = DB.<Entity>select()
			.from("TEST")
			.join("OTHER").on("ID", "AID")
			.leftJoin("ANOTHER").on("ID")			
			.bind("COLUMN1", Entity::setColumn1)
			.bind("COLUMN2", Entity::setColumn2)
 			.where("COLUMN3 = '' AND COLUMN4 IS NOT NULL")
 			.orderBy("COLUMN1")
 			.distinct()
			.reifyFirst();
	}
	
	@Test
	public void test4() {
		//SELECT COLUMN1, COLUMN2 FROM TEST T
		//LEFT JOIN ANOTHER A ON A.SOMEFILED = T.SOMEFILED 		
		//WHERE COLUMN3 = '' AND COLUMN4 IS NOT NULL
		//ORDER BY COLUMN1
 				
		List<ResultMap> data = DB.selectMapped()
			.from("TEST")
			.leftJoin("ANOTHER").on("SOMEFILED")
 			.get("COLUMN1", ResultMap.STRING)
 			.get("COLUMN2", ResultMap.INTEGER)
  			.where("COLUMN3 = '' AND COLUMN4 IS NOT NULL")
			.reify();
	}
	
	@Test
	public void test5() {
		//SELECT DISTINCT COLUMN1 FROM TEST T
		//JOIN OTHER O ON O.AID = T.AID 
 		//WHERE COLUMN3 = '' AND COLUMN4 IS NOT NULL
		//LIMIT 1
		
		ResultMap data = DB.selectMapped()
			.from("TEST")
			.join("OTHER").on("AID")
 			.get("COLUMN1", ResultMap.STRING)
 			.get("COLUMN2", ResultMap.INTEGER)
  			.where("COLUMN3 = '' AND COLUMN4 IS NOT NULL")
 			.distinct()
			.reifyFirst();		
		
		String column1 = data.get("COLUMN1");
		Integer column2 = data.get("COLUMN2");
	}
}
