package dc.lambda;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

public class ResultMap {
	private final Map<String, ?> values = new HashMap<>();

	public static final MappingResult<BigDecimal> BIGDECIMAL = use(ResultSet::getBigDecimal);
	public static final MappingResult<Timestamp> TIMESTAMP   = use(ResultSet::getTimestamp);
	public static final MappingResult<Boolean> BOOLEAN       = use(ResultSet::getBoolean);
	public static final MappingResult<String> STRING         = use(ResultSet::getString);
	public static final MappingResult<Integer> INTEGER       = use(ResultSet::getInt);
	public static final MappingResult<Long> LONG             = use(ResultSet::getLong);
	public static final MappingResult<Date> DATE             = use(ResultSet::getDate);

	public <T> T get(String column) {
 		return (T) values.get(column);
	}
	
	private static <R> BiFunction<ResultSet, String, Optional<R>> safe(UnsafeExtractor<R> getter) {
		return (result, column) -> {
			try {
				return Optional.ofNullable(getter.extract(result, column));
			} catch (SQLException e) {
				return Optional.empty();
			}
		};
	}

	private static <R> MappingResult<R> use(UnsafeExtractor<R> getter) {
		return (R, column) -> safe(getter).apply(R, column);
	}

	@FunctionalInterface
	interface MappingResult<R> {
		Optional<R> get(ResultSet result, String column);
	}

	@FunctionalInterface
	private interface UnsafeExtractor<R> {
		R extract(ResultSet result, String column) throws SQLException;
	}
}
