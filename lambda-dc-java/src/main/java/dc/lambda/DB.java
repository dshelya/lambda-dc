package dc.lambda;

import java.util.ArrayList;
import java.util.Collection;

public final class DB {
	private DB() {
	}

	public static final <T> SelectStatement<T> selectInto(Collection<T> container) {
		return new SelectStatement<>(container);
	}

	public static final <T> SelectStatement<T> select() {
		return new SelectStatement<>(new ArrayList<T>());
	}

	public static SelectMapStatement selectMapped() {
		return new SelectMapStatement();
	}
}