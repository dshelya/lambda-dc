package dc.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import dc.lambda.ResultMap.MappingResult;

public class SelectMapStatement implements Statement {
	private Collection<ResultMap> resultContainer;
	
	private Map<String, MappingResult<?>> mappers;
	private Map<String, Function<ResultMap, String>> conditions;

	private List<Join> joins;
	private String table;
	private String orderByColumns;
 	private boolean distinct;

	public SelectMapStatement() {		
		mappers = new LinkedHashMap<>();
		conditions = new LinkedHashMap<>();
	}

	public SelectMapStatement from(String table) {
		this.table = table;
		return this;
	}

	public <R> SelectMapStatement get(String column, MappingResult<R> mapper) {
		mappers.put(column, mapper);
 		return this;
	}
	
	public Join leftJoin(String table) {
		return new Join(this, table, true);
	}
	
	public Join join(String table) {
		return new Join(this, table, false);
	}
	
 	public SelectMapStatement where(String condition) {
		conditions.put(condition, Z -> condition);		
		return this;
	}
	
	public SelectMapStatement orderBy(String orderByColumns) {
		this.orderByColumns = orderByColumns;
 		return this;
	}
	
	public SelectMapStatement distinct() {
		distinct = true;
 		return this;
	}

	public List<ResultMap> reify() {
		Arrays.asList("");
 		return new ArrayList<>();
	}
	
	public final class Join {
		private SelectMapStatement select;

		private boolean isLeft;
		private String join;
		private String table;

		private Join(SelectMapStatement select, String table, boolean isLeft) {
			this.select = select;
			this.table = table;
			this.isLeft = isLeft;
		}
		
		public SelectMapStatement on(String baseColumn, String joinedColumn) {
			String base = select.table;
			this.join = String.format("%s JOIN %s ON %s.%s = %s.%s", isLeft ? "LEFT" : "", table, base, baseColumn, table, joinedColumn);
			
			joins.add(this);
			return select;
		}
		
		public SelectMapStatement on(String sameColumn) {
			return on(sameColumn, sameColumn);
		}
	}

	public ResultMap reifyFirst() {
		return null;
	}
}

