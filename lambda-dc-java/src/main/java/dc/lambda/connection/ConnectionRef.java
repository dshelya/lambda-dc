package dc.lambda.connection;

public final class ConnectionRef {
	private String name;
 
	ConnectionRef(String refName) {
		this.name = refName;
 	}
	
	public void setAsDefault() {
		ConnectionMetadata.setDefault(this);
	} 

	@Override
	public int hashCode() {		
		return 57 + name == null ? 0 : name.hashCode();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		
		if (that == null || !(that instanceof ConnectionRef)) {
			return false;
		}
		
		ConnectionRef other = (ConnectionRef) that;
		
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		
		return true;
	} 
}
