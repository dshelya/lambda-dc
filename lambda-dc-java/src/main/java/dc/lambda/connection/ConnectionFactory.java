package dc.lambda.connection;

import dc.lambda.connection.factories.DatabaseFactoryInitException;

public interface ConnectionFactory {
	default String type() {
		return this.getClass().getSimpleName();
	}

	void initialize() throws DatabaseFactoryInitException;
}
