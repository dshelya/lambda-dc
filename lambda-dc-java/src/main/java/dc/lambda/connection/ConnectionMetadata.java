package dc.lambda.connection;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import dc.lambda.connection.factories.DatabaseFactoryInitException;
import dc.lambda.connection.factories.H2FileFactory;
import dc.lambda.connection.factories.H2InMemoryFactory;

public final class ConnectionMetadata {
	private final static Map<ConnectionRef, ConnectionFactory> FACTORIES = new HashMap<>();;
	private final static Map<String, Integer> INDICES = new HashMap<>();
	private static Optional<ConnectionFactory> defaultFactory = Optional.empty();

	private ConnectionMetadata() {}
	
	public static ConnectionRef registerH2FileDatabase(String h2DbFilePath) throws DatabaseFactoryInitException {
		return register(H2FileFactory.reify(h2DbFilePath));
	}
	
	public static ConnectionRef registerH2InMemoryDatabase() throws DatabaseFactoryInitException {
		return register(H2InMemoryFactory.reify());
	}
	
	public static ConnectionRef register(ConnectionFactory factory) throws DatabaseFactoryInitException {
		return register(factory, false);
	}

	public static ConnectionRef register(ConnectionFactory factory, boolean isDefault) throws DatabaseFactoryInitException {
		factory.initialize();

		Integer refIndex = INDICES.getOrDefault(factory.type(), 0) + 1;
		String refName = factory.type() + refIndex;

		ConnectionRef referecence = new ConnectionRef(refName);
		FACTORIES.put(referecence, factory);

		if (isDefault || !defaultFactory.isPresent()) {
			defaultFactory = Optional.ofNullable(factory);
		}		

		return referecence;
	}

	public static Optional<ConnectionFactory> getDefault() {
		return defaultFactory;
	}

	public static Optional<ConnectionFactory> getByRefference(ConnectionRef ref) {
		return Optional.ofNullable(FACTORIES.get(ref));
	}
	
	public static Set<ConnectionRef> list() {
		return Collections.unmodifiableSet(FACTORIES.keySet());
	}
	
	public static void setDefault(ConnectionRef reference) {
		defaultFactory = Optional.ofNullable(FACTORIES.get(reference));;
	} 
}
