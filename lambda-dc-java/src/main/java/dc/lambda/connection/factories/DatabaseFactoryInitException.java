package dc.lambda.connection.factories;

public class DatabaseFactoryInitException extends Exception {
	private static final long serialVersionUID = -1961933424284873735L;

	public DatabaseFactoryInitException(Exception cause) {
		super(cause);
	}
}
