package dc.lambda.connection.factories;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

import org.apache.commons.dbcp2.BasicDataSource;
import dc.lambda.connection.ConnectionFactory;

public abstract class GenericFactory implements ConnectionFactory {
	private BasicDataSource connectionPool;

	protected String driverName;
	protected String jdbcUrl;

	protected GenericFactory(String driverName, String jdbcUrl) {
		this.driverName = driverName;
		this.jdbcUrl = jdbcUrl;
	}

	public Optional<Connection> getConnection() {
		try {
			return Optional.of(connectionPool.getConnection());
		} catch (SQLException e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public void initialize() throws DatabaseFactoryInitException {
		try {
			Class.forName(driverName);

			connectionPool = new BasicDataSource();

			connectionPool.setUrl(jdbcUrl);
			connectionPool.setMinIdle(20);
			connectionPool.setMaxIdle(100);
			connectionPool.setTimeBetweenEvictionRunsMillis(120000);
			connectionPool.setMaxOpenPreparedStatements(100);
		} catch (ClassNotFoundException e) {
			throw new DatabaseFactoryInitException(e);
		}
	}
}
