package dc.lambda.connection.factories;

public class H2InMemoryFactory extends GenericFactory {
	private static final String CLASS_NAME = "org.h2.Driver";
	private static final String DEFAULT_DB = "session";
	private static final String DEFAULT_URL = "jdbc:h2:mem:%s;DB_CLOSE_DELAY=-1";

	private H2InMemoryFactory(String driverName, String jdbcUrl) {
		super(driverName, jdbcUrl);		
	}

	public static H2InMemoryFactory reify(String databaseName) {
  		return new H2InMemoryFactory(CLASS_NAME, String.format(DEFAULT_URL, databaseName));
	}
	
	public static H2InMemoryFactory reify() {
  		return new H2InMemoryFactory(CLASS_NAME, String.format(DEFAULT_URL, DEFAULT_DB));
	}
}
