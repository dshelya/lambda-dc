package dc.lambda.connection.factories;

public class H2FileFactory extends GenericFactory {
	private static final String CLASS_NAME = "org.h2.Driver";
	private static final String DEFAULT_URL = "jdbc:h2:%s;AUTO_SERVER=TRUE;AUTO_RECONNECT=TRUE;FILE_LOCK=SOCKET";

	private H2FileFactory(String driverName, String jdbcUrl) {
		super(driverName, jdbcUrl);		
	}

	public static H2FileFactory reify(String filePath) {
  		return new H2FileFactory(CLASS_NAME, String.format(DEFAULT_URL, filePath));
	}
}
