package dc.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

import dc.lambda.connection.ConnectionFactory;
import dc.lambda.connection.ConnectionMetadata;
import dc.lambda.connection.ConnectionRef;

public final class SelectStatement<T> implements Statement {
	private Collection<T> resultContainer;
	
	private Map<String, BiConsumer<T, ?>> setters;
	private Map<String, Function<T,String>> conditions;

	private List<Join> joins;
	private String table;
	private String orderByColumns;

	private boolean distinct;

	private Optional<ConnectionFactory> database;

	SelectStatement(Collection<T> resultContainer) {
		database = ConnectionMetadata.getDefault();	
		setters = new LinkedHashMap<>();
		conditions = new LinkedHashMap<>();
		joins = new ArrayList<>();
		
		this.resultContainer = resultContainer;
	}

	public SelectStatement<T> from(String table) {
		this.table = table;
		return this;
	}

	public <V> SelectStatement<T> bind(String column, BiConsumer<T, V> setter) {
		setters.put(column, setter);
		return this;
	}
	
	public Join leftJoin(String table) {
		return new Join(this, table, true);
	}
	
	public Join join(String table) {
		return new Join(this, table, false);
	}
	
 	public SelectStatement<T> where(String condition) {
		conditions.put(condition, Z -> condition);		
		return this;
	}
	
	public SelectStatement<T> orderBy(String orderByColumns) {
		this.orderByColumns = orderByColumns;
 		return this;
	}
	
	public SelectStatement<T> distinct() {
		distinct = true;
 		return this;
	}
	
	public SelectStatement<T> use(ConnectionRef reference) {
		database = ConnectionMetadata.getByRefference(reference);
 		return this;
	}

	public List<T> reify() {
		Arrays.asList("");
 		return new ArrayList<>();
	}
	
	public T reifyFirst() {
		return null;
	}
	
	public final class Join {
		private SelectStatement<T> select;

		private boolean isLeft;
		private String join;
		private String table;

		private Join(SelectStatement<T> select, String table, boolean isLeft) {
			this.select = select;
			this.table = table;
			this.isLeft = isLeft;
		}
		
		public SelectStatement<T> on(String baseColumn, String joinedColumn) {
			String base = select.table;
			this.join = String.format("%s JOIN %s ON %s.%s = %s.%s", isLeft ? "LEFT" : "", table, base, baseColumn, table, joinedColumn);
			
			joins.add(this);
			return select;
		}
		
		public SelectStatement<T> on(String sameColumn) {
			return on(sameColumn, sameColumn);
		}
	}
}
